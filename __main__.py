""" main function to drive program """
# recipe/__main__.py
# this module allows to run the package with `python3 -m recipe...`
import logging
import sys

from bs4 import BeautifulSoup

# local imports
from recipe.util import httpUtil
from recipe import parser
from recipe import ingredients
from recipe.util import formattingUtil

logging.basicConfig(filename='recipe.log', level=logging.INFO)

if len(sys.argv) <= 1:
    print('Usage: python3 -m recipe [url1] [url2]...')
    sys.exit(1)

urls = []
recipeIngredients = []

for url in sys.argv[1:]:
    host, path = httpUtil.transformUrl(url)
    urls.append({'host': host, 'path': path})

for url in urls:
    pageHtml = httpUtil.retrievePageHtml(url)
    parsedHtml = BeautifulSoup(pageHtml, 'html.parser')
    recipeIngredients.append(parser.parseRecipeIngredients(parsedHtml))

merged = ingredients.mergeIngredientLists(recipeIngredients)
formattingUtil.prettyPrintItems(merged.items())
