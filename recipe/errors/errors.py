""" Module for housing different errors """

from recipe.errors import Error

class PageRetrievalError(Error):
    """Exception raised when there is an error making the HTML request for a given URL.

    Attributes:
        url -- the url that was being retrieved
    """

    def __init__(self, url):
        self.url = url
        self.message = "Unexpected error when retrieving URL: " + url
