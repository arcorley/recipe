""" module for ingredient operations """
# local imports
from . import conversion


def createIngredientEntry(ingredient, duplicate):
    """
    Creates a finished ingredient entry and returns the name
    along with the parsed data for that ingredient

    Args:
        ingredient (dict): dict containing all data extracted from ingredient
        duplicate (bool): flag stating whether this ingredient already exists
            in the processed list of ingredients

    Returns:
        nameStr (string): the key that should be used for this entry
        data (dict): dict containing ingredient info that was present after
            parsing
    """
    data = {
        'unit': None,
        'amount': None,
        'notes': []
    }

    if 'unit' in ingredient:
        data['unit'] = ingredient['unit']

    if 'amount' in ingredient:
        data['amount'] = ingredient['amount']

    if 'notes' in ingredient:
        data['notes'] = ingredient['notes']

    nameStr = ingredient['name']

    if duplicate:
        nameStr += ' duplicate'

    return nameStr, data

def mergeIngredientLists(lists):
    """
    Merges the ingredient lists for all recipes provided to program

    Args:
        lists (array of dict): array of ingredient lists

    Returns:
        merged (dict): a dict containing one entry per ingredient found across
            all recipes and their corresponding data
    """
    merged = {}
    # loop through each recipe, then each ingredient and add items
    for recipe in lists:
        for ingredient in recipe:
            name = ingredient['name']
            matchingKey = findMatchingEntryKey(merged, name)

            # Already encountered this ingredient, merge the entries.
            if matchingKey is not None:
                largerUnit = None
                existing = merged[matchingKey]

                if existing['unit'] is not None and ingredient['unit'] is not None:
                    amount = existing['amount'] or 0

                    if existing['unit'] != ingredient['unit']:
                        amount, largerUnit = conversion.mergeMeasurements(existing, ingredient)
                    else:
                        amount += (ingredient['amount'] or 0)

                    existing['amount'] = amount

                    if largerUnit and (largerUnit != existing['unit']):
                        existing['unit'] = largerUnit

                    # Combine the notes from the two entries
                    if ingredient['notes']:
                        existing['notes'] += ingredient['notes']
            else:
                entryName, entryData = createIngredientEntry(ingredient, False)
                merged[entryName] = entryData

    return merged

def findMatchingEntryKey(entryDict, name):
    """
    Finds the matching entry in the dict

    Args:
        entryDict: current dict of ingredients
        name: the name we're looking for in the entryDict

    Returns:
        the matching entry
    """
    for key in entryDict.keys():
        if name in key or key in name:
            return key

    return None
