""" util module for various numeric manipulation """
import logging

from fractions import Fraction

logger = logging.getLogger(__name__)

def convertAmountToNumeric(val):
    """ convert recipe amount into common format """

    isNumeric, convertedVal = checkIntOrFloat(val)

    if isNumeric:
        return convertedVal

    # if value is a string, need to look for fractions
    if isinstance(val, str):
        # look for fractions split by space or hyphen
        spaceArr = val.split(' ')
        dashArr = val.split('-')
        vals = []
        if len(spaceArr) > 1:
            vals = spaceArr
        elif len(dashArr) > 1:
            vals = dashArr
        else:
            vals.append(val)

        runningTotal = 0

        for entry in vals:
            converted = convertStrToNumericType(entry)
            if converted is not None:
                runningTotal += converted

        return runningTotal

    return None

def checkIntOrFloat(val):
    """ checks if a value is an int or float returns boolean and the converted value """
    if isinstance(val, int):
        return True, int(val)

    if isinstance(val, float):
        return True, float(val)

    return False, None

def convertStrToNumericType(val):
    """ returns the numeric equivalent of provided string """

    # try converting string into a fraction, swallow error
    try:
        return float(Fraction(val))
    except ValueError:
        logger.error('Unable to convert %s to numeric', val)

    return None
