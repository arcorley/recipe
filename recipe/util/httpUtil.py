""" module for various http operations """
from urllib.parse import urlparse
import http.client

# local imports
from recipe.errors import errors

def transformUrl(url):
    """ transform url from raw string"""
    parsedUrl = urlparse(url)
    host = None
    path = None

    try:
        if parsedUrl.netloc == '' or parsedUrl.path == '':
            raise ValueError

        host = parsedUrl.netloc
        path = parsedUrl.path
        return host, path
    except ValueError as ex:
        print('Error parsing provided input URL: ' + url)
        raise ValueError from ex

def retrievePageHtml(url):
    """ make get request for website html"""
    try:
        conn = http.client.HTTPSConnection(url['host'])
        conn.request("GET", url['path'])
        return conn.getresponse().read()
    except Exception as ex:
        raise errors.PageRetrievalError(url) from ex
