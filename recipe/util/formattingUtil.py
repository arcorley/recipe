""" Module for formatting various recipe data """
def prettyPrintItems(mergedItems):
    """
    Prints ingredient list in human readable format.

    Args:
        mergedItems (ingredient dict ItemsView)
    """
    for key, value in mergedItems:
        amount = ''
        unit = ''

        if value['amount'] is not None:
            amount = str(value['amount'])

        if value['unit'] is not None:
            unit = value['unit']

        print(amount + ' ' + unit + ' ' + key)
