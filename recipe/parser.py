""" module contains logic for parsing recipe page """
import logging

from bs4 import BeautifulSoup

# local imports
from recipe import conversion
from recipe.util import conversionUtil

logger = logging.getLogger(__name__)

# TODO: break this function up into smaller pieces
def parseRecipeIngredients(soup):
    """
    Uses BeautifulSoup to parse out relevant ingredient data

    Args:
        soup (BeautifulSoup Object): HTML retrieved from provided URL

    Returns:
        ingredients (array of ingredient dict)
    """
    # get an array of all the ingredients on page
    ingredientsHtml = soup.find_all('li', {"class": "wprm-recipe-ingredient"})
    ingredients = []

    for ingredient in ingredientsHtml:
        parsedIngredient = {
            'name': None,
            'unit': None,
            'amount': None,
            'notes': []
        }
        localSoup = BeautifulSoup(str(ingredient), 'html.parser')

        name = localSoup.select('.wprm-recipe-ingredient-name')
        unit = localSoup.select('.wprm-recipe-ingredient-unit')
        amount = localSoup.select('.wprm-recipe-ingredient-amount')
        notes = localSoup.select('.wprm-recipe-ingredient-notes')

        if len(name) > 0:
            parsedIngredient['name'] = name[0].text.lower()

        if len(unit) > 0:
            unitName = conversion.convertToStandardUnit(unit[0].text.lower())

            try:
                parsedIngredient['unit'] = unitName
            except ValueError:
                logger.error('Unable to map unit: %s', unit[0].text.lower())

        if len(amount) > 0:
            amountText = amount[0].text.lower()
            parsedIngredient['amount'] = conversionUtil.convertAmountToNumeric(amountText)

        if len(notes) > 0:
            parsedIngredient['notes'] = []
            for note in notes:
                parsedIngredient['notes'].append(note.text.lower())

        ingredients.append(parsedIngredient)

    return ingredients
