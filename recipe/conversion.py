""" module to normalize and convert units """
import logging

logger = logging.getLogger(__name__)

unitNames = {
    'cup': 'cup',
    'cups': 'cup',
    'c': 'cup',
    'C': 'cup',
    'dash': 'tsp',
    'pound': 'lb',
    'pounds': 'lb',
    'g': 'g',
    'gram': 'g',
    'grams': 'g',
    'lb': 'lb',
    'lbs': 'lb',
    'ounce': 'oz',
    'ounces': 'oz',
    'oz': 'oz',
    'ozs': 'oz',
    'pinch': 'tsp',
    'Pinch': 'tsp',
    'pint': 'pint',
    'pt': 'pint',
    'quart': 'quart',
    'qt': 'quart',
    't': 'tsp',
    'tsp': 'tsp',
    'teaspoon': 'tsp',
    'Teaspoon': 'tsp',
    'T': 'Tbsp',
    'tablespoon': 'Tbsp',
    'tablespoons': 'Tbsp',
    'TB': 'Tbsp',
    'Tbl': 'Tbsp',
    'Tbsp': 'Tbsp',
    'tbsp': 'Tbsp'
}

conversions = {
    'cup': {
        'oz': 8,
        'pint': 0.5,
        'quart': 0.25,
        'Tbsp': 16,
        'tsp': 48
    },
    'g': {
        'cup': 0.005,
        'lb': 0.0022,
        'oz': 0.035274,
        'tsp': 0.2,
        'Tbsp': 0.0714286
    },
    'lb': {
        'g': 453.592,
        'oz': 16
    },
    'oz': {
        'cup': 0.125,
        'quart': 0.03125,
        'Tbsp': 2,
        'tsp': 6
    },
    'pint': {
        'cup': 2,
        'oz': 16,
        'quart': 0.5,
        'Tbsp': 32,
        'tsp': 96
    },
    'quart': {
        'cup': 4,
        'oz': 32,
        'pint': 2,
        'Tbsp': 64,
        'tsp': 192
    },
    'Tbsp': {
        'cup': 0.0625,
        'oz': 0.5,
        'pint': 0.03125,
        'quart': 0.015625,
        'tsp': 3
    },
    'tsp': {
        'cup': 0.0208333,
        'oz': 0.1666663,
        'pint': 0.0104166,
        'quart': 0.0052083,
        'Tbsp': 0.33
    }
}


def convertToStandardUnit(unit):
    """ convert unit to standard form """
    try:
        return unitNames[unit]
    except KeyError:
        logger.warning('Unable to convert unit %s to standard unit', unit)
        return None


def mergeMeasurements(measure1, measure2):
    """ merge multiple measurements into a single amount """
    if 'amount' in measure1 and 'amount' in measure2:
        largerUnit = findLargerUnit(measure1, measure2)

        if measure1['unit'] == largerUnit:
            smallerUnit = measure2['unit']
            converted = round(conversions[smallerUnit][largerUnit] * (measure2['amount'] or 1), 1)
            return converted + (measure1['amount'] or 1), largerUnit
        else:
            smallerUnit = measure1['unit']
            converted = round(conversions[smallerUnit][largerUnit] * (measure1['amount'] or 1), 1)
            return converted + (measure2['amount'] or 1), largerUnit

    return None, None


def findLargerUnit(measure1, measure2):
    """ find larger unit of two measurements """
    aConversions = conversions[measure1['unit']]
    bConversions = conversions[measure2['unit']]
    if aConversions[measure2['unit']] > bConversions[measure1['unit']]:
        return measure1['unit']
    else:
        return measure2['unit']
