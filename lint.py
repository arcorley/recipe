""" run linter """
import sys
from pylint import lint

threshold = 9.0

run = lint.Run([sys.argv[1]], do_exit=False)
score = run.linter.stats['global_note']

if score < threshold:
    sys.exit(1)
