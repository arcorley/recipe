""" tests for parser class """
import unittest
from bs4 import BeautifulSoup

# local imports
from recipe.util import httpUtil
from recipe import parser

testUrl = "https://www.skinnytaste.com/quick-cabbage-slaw/"
host, path = httpUtil.transformUrl(testUrl)
pageHtml = httpUtil.retrievePageHtml({'host': host, 'path': path})
soup = BeautifulSoup(pageHtml, 'html.parser')

class testParser(unittest.TestCase):
    """ tests for parser class """

    def testParseRecipeIngredientsValidInputReturnsIngredients(self):
        """ returns ingredient list with valid input """
        ingredients = parser.parseRecipeIngredients(soup)
        self.assertGreater(len(ingredients), 0)

if __name__ == '__main__':
    unittest.main()
