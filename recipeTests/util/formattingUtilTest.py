""" tests for formatting util """
import unittest
import sys

from io import StringIO

# local imports
from recipe.util import formattingUtil

class formattingUtilTest(unittest.TestCase):
    """ tests for formatting util """
    def testPrintingFormattedAsExpected(self):
        """ prints to console as expected """
        ingredients = {
            'onion': {'amount': 1, 'unit': 'cup'},
            'ground beef': {'amount': 1, 'unit': 'lb'}}
        expectedStr = '1 cup onion\n1 lb ground beef\n'
        capturedOutput = StringIO()          # Create StringIO object
        sys.stdout = capturedOutput          #  and redirect stdout.
        formattingUtil.prettyPrintItems(ingredients.items())
        sys.stdout = sys.__stdout__          # Reset redirect.
        self.assertEqual(expectedStr, capturedOutput.getvalue())

if __name__ == '__main__':
    unittest.main()
