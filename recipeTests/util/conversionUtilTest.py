""" tests for util.py """
import logging
import unittest
import unittest.mock

# local imports
from recipe.util import conversionUtil

class testUtil(unittest.TestCase):
    """ tests for util functions """

    def testConvertAmountToNumericReturnsIntWhenPassedInt(self):
        """ return an integer if given an integer """
        self.assertEqual(42, conversionUtil.convertAmountToNumeric(42))

    def testConvertAmountToNumericReturnsFloatWhenPassedFloat(self):
        """  return a float if given a float """
        self.assertEqual(79.2, conversionUtil.convertAmountToNumeric(79.2))

    def testConvertAmountToNumericReturnsFloatWhenPassedFraction(self):
        """ convert fraction to a float """
        self.assertEqual(0.50, conversionUtil.convertAmountToNumeric('1/2'))

    def testConvertAmountToNumericReturnsNoneWithBadInput(self):
        """ return none with bad input """
        self.assertEqual(None, conversionUtil.convertAmountToNumeric(2+3j))

    def testConvertAmountToNumericReturnsFloatWhenPassedMixedFractionSpaceSeparated(self):
        """ convert space separated mixed fraction to a float """
        self.assertEqual(1.5, conversionUtil.convertAmountToNumeric('1 1/2'))

    def testConvertAmountToNumericReturnsFloatWhenPassedMixedFractionDashSeparated(self):
        """ convert dash separated mixed fraction to a float """
        self.assertEqual(3.5, conversionUtil.convertAmountToNumeric('3-1/2'))

    def testconvertStrToNumericTypeThrowsWhenPassedBadInput(self):
        """ test convert str to numeric with bad input """
        with self.assertLogs(logging.getLogger('recipe')) as context:
            conversionUtil.convertStrToNumericType('3/2/3')

        expectedString = "ERROR:recipe.util.conversionUtil:Unable to convert 3/2/3 to numeric"
        self.assertIn(expectedString, context.output)


if __name__ == '__main__':
    unittest.main()
