""" tests for http util """
import unittest

# local imports
from recipe.util import httpUtil

class httpUtilTest(unittest.TestCase):
    """ tests for http util """
    def testTransformUrlThrowsWithBadInput(self):
        """ throws ValueError when provided bad input """
        with self.assertRaises(ValueError):
            httpUtil.transformUrl("badinput")

    def testTransformUrlReturnsWithValidInput(self):
        """ returns valid data when provided good input """
        testUrl = "https://testhost.com/some/path"
        host, path = httpUtil.transformUrl(testUrl)
        self.assertEqual(host, "testhost.com")
        self.assertEqual(path, "/some/path")

if __name__ == '__main__':
    unittest.main()
    