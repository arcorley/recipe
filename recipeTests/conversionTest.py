""" tests for conversion.py """
import unittest
import logging
from recipe import conversion

class testConversion(unittest.TestCase):
    """ tests for conversion file """

    def testConvertToStandardUnitPassesWithValidUnit(self):
        """ converts to common format with valid input """
        unit = 'pound'
        self.assertEqual(conversion.convertToStandardUnit(unit), "lb")

    def testConvertToStandardUnitReturnsNoneWithUnknownUnit(self):
        """ returns None when given invalid input """
        unit = 'someNonexistentUnit'
        with self.assertLogs(logging.getLogger('recipe')) as context:
            result = conversion.convertToStandardUnit(unit)

        self.assertEqual(result, None)
        self.assertIn('WARNING:recipe.conversion:Unable to convert unit someNonexistentUnit to standard unit', context.output)

    def testMergeMeasurementsValidInputLeftSideGreaterCase(self):
        """ case where measurement1 has the larger unit type """
        measurement1 = {
            'amount': 2,
            'unit': 'Tbsp'
        }

        measurement2 = {
            'amount': 3,
            'unit': 'tsp'
        }

        amount, unit = conversion.mergeMeasurements(measurement1, measurement2)

        self.assertEqual(amount, 3.0)
        self.assertEqual(unit, 'Tbsp')

    def testMergeMeasurementsValidInputRightSideGreaterCase(self):
        """ case where measurement2 has the larger unit type """
        measurement1 = {
            'amount': 8,
            'unit': 'oz'
        }

        measurement2 = {
            'amount': 2,
            'unit': 'cup'
        }

        amount, unit = conversion.mergeMeasurements(measurement1, measurement2)

        self.assertEqual(amount, 3.0)
        self.assertEqual(unit, 'cup')


    def testMergeMeasurementsValidInputNoAmountFieldReturnsNone(self):
        """ case where no amount field is in a measurement """

        measurement1 = {}
        measurement2 = {
            'amount': 2,
            'unit': 'cup'
        }

        amount, unit = conversion.mergeMeasurements(measurement1, measurement2)

        self.assertEqual(amount, None)
        self.assertEqual(unit, None)

if __name__ == '__main__':
    unittest.main()
