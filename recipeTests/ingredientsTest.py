""" tests for ingredients module """
import unittest

# local imports
from recipe import ingredients

class ingredientsTest(unittest.TestCase):
    """ tests for ingredients module """
    def testCreateIngredientEntryValidInputReturnsIngredient(self):
        """ create ingredient entry with valid input """
        data = {
            'name': 'testname',
            'unit': 'cup',
            'amount': 2,
            'notes': 'testnotestr'
        }

        name, ingredientData = ingredients.createIngredientEntry(data, False)
        self.assertEqual('testname', name)
        self.assertEqual('cup', ingredientData['unit'])
        self.assertEqual(2, ingredientData['amount'])
        self.assertEqual('testnotestr', ingredientData['notes'])

    def testCreateIngredientEntryValidDuplicateInputReturnsIngredient(self):
        """ create duplicate ingredient entry with valid input """
        data = {
            'name': 'testname',
            'unit': 'Tbsp',
            'amount': 3,
            'notes': 'testnotestr'
        }

        name, ingredientData = ingredients.createIngredientEntry(data, True)
        self.assertEqual('testname duplicate', name)
        self.assertEqual('Tbsp', ingredientData['unit'])
        self.assertEqual(3, ingredientData['amount'])
        self.assertEqual('testnotestr', ingredientData['notes'])

    def testMergeIngredientListsValidInputReturnsMergedDictSameUnitCase(self):
        """ return merged dict of ingredients (same unit merge) """
        ingredientList = [
            [
                {
                    'name': 'olive oil',
                    'unit': 'Tbsp',
                    'amount': 2,
                    'notes': []
                },
                {
                    'name': 'onion',
                    'unit': '',
                    'amount': 1,
                    'notes': []
                }
            ],
            [
                {
                    'name': 'olive oil',
                    'unit': 'Tbsp',
                    'amount': 1,
                    'notes': []
                }
            ]
        ]

        expectedResult = {
            'olive oil': {
                'unit': 'Tbsp',
                'amount': 3,
                'notes': []
            },
            'onion': {
                'unit': '',
                'amount': 1,
                'notes': []
            }
        }

        self.assertDictEqual(expectedResult, ingredients.mergeIngredientLists(ingredientList))

    def testMergeIngredientListsValidInputReturnsMergedDictDiffUnitExistingLargerCase(self):
        """ return merged dict of ingredients (diff unit, existing larger merge) """
        ingredientList = [
            [
                {
                    'name': 'olive oil',
                    'unit': 'cup',
                    'amount': 1,
                    'notes': []
                },
                {
                    'name': 'onion',
                    'unit': '',
                    'amount': 1,
                    'notes': []
                }
            ],
            [
                {
                    'name': 'olive oil',
                    'unit': 'Tbsp',
                    'amount': 16,
                    'notes': []
                },
                {
                    'name': 'cumin',
                    'unit': 'Tbsp',
                    'amount': 2,
                    'notes': ['test note']
                }
            ],
            [
                {
                    'name': 'olive oil',
                    'unit': 'oz',
                    'amount': 8,
                    'notes': []
                },
                {
                    'name': 'cumin',
                    'unit': 'tsp',
                    'amount': 3,
                    'notes': ['test note 2']
                }
            ]
        ]

        expectedResult = {
            'olive oil': {
                'unit': 'cup',
                'amount': 3.0,
                'notes': []
            },
            'onion': {
                'unit': '',
                'amount': 1,
                'notes': []
            },
            'cumin': {
                'unit': 'Tbsp',
                'amount': 3.0,
                'notes': ['test note', 'test note 2']
            }
        }

        self.assertDictEqual(expectedResult, ingredients.mergeIngredientLists(ingredientList))

    def testMergeIngredientListsValidInputReturnsMergedDictDiffUnitExistingSmallerCase(self):
        """ return merged dict of ingredients (diff unit, existing smaller merge) """
        ingredientList = [
            [
                {
                    'name': 'olive oil',
                    'unit': 'Tbsp',
                    'amount': 16,
                    'notes': []
                },
                {
                    'name': 'onion',
                    'unit': '',
                    'amount': 1,
                    'notes': []
                }
            ],
            [
                {
                    'name': 'olive oil',
                    'unit': 'cup',
                    'amount': 2,
                    'notes': []
                },
                {
                    'name': 'cumin',
                    'unit': 'tsp',
                    'amount': 6,
                    'notes': []
                }
            ],
            [
                {
                    'name': 'cumin',
                    'unit': 'Tbsp',
                    'amount': 1,
                    'notes': ['test note', 'test note 2']
                }
            ]
        ]

        expectedResult = {
            'olive oil': {
                'unit': 'cup',
                'amount': 3.0,
                'notes': []
            },
            'onion': {
                'unit': '',
                'amount': 1,
                'notes': []
            },
            'cumin': {
                'unit': 'Tbsp',
                'amount': 3.0,
                'notes': ['test note', 'test note 2']
            }
        }

        self.assertDictEqual(expectedResult, ingredients.mergeIngredientLists(ingredientList))
        
if __name__ == '__main__':
    unittest.main()
